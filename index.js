const express = require('express')
const app = express()
const port = process.env.PORT || 8080

app.get('/', (req, res) => {
  res.send('Hi,How are you all?, I am Sai Shrikar')
})

app.listen(port, '0.0.0.0', () => {
  console.log(`Example app listening at http://'0.0.0.0':${port}`)
})